export default {
    title: 'abdulrahman',
    email: 'abdulrahman.8alah@gmail.com',
    description: `Hello, my name is Abdulrahman. I am a software engineer and freelancer.
    I'm currently working as a freelancer these days. I've built several open source projects.
    I'm also looking for full-time or part-time work. 👀`,
    donate: 'https://liberapay.com/abdulrahman',
    linkedin: 'https://linkedin.com/in/abdulrahmann'
}
